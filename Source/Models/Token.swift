//
//  Token.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 18/08/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import BigInt
import os.log

/// A class to represent a Token on the Tezos network. This class will do all the heavy lifting of converting values from the RPC to more human readbale values.
/// This class will also handle arithmetic functions, allowing developers to add and subtract tokens (useful when caluclating fees and total values).
public class Token {
	
	/// An Enum to express the type of a token. Different processes are needed to fetch a balance for a users XTZ wallet,
	/// versus fetching a FA1.2 token balance. This allows the library to abstract this logic away from the user and handle it all behind the scenes.
	public enum TokenType: String {
		case xtz
		case fa1_2
	}
	
	/// The icon used to display next to a given token.
	public var icon: UIImage?
	
	/// The long name of a token. e.g. "Tezos".
	public let name: String
	
	/// The short name or the symbol of a token. e.g. "XTZ".
	public let symbol: String
	
	/// The type of this token. e.g. xtz or fa1.2.
	public let tokenType: TokenType
	
	/// Object that holds and formats the balance of the token
	public var balance: TokenAmount
	
	/// The current local currency rate of this token. Used to show the user the net worth of their holdings.
	public var localCurrencyRate: Decimal = 0
	
	/// In the case of FA1.2 or higher, we need to know the KT1 address for the token so we can fetch balances and make trades. (should be empty for xtz).
	public let tokenContractAddress: String?
	
	/// In the case of FA1.2 or higher, we need to know the KT1 address used by dexter, for making exchanges for this token. (should be empty for xtz).
	public let dexterExchangeAddress: String?
	
	/// How much XTZ is Dexter holding in liquidity, for the token pair  (used to calculate prices and exchange rates)
	public var dexterXTZPool: XTZAmount = XTZAmount.zero()
	
	/// How much of this token is Dexter holding in liquidity, for the token pair (used to calculate prices and exchange rates)
	public var dexterTokenPool: TokenAmount = TokenAmount.zeroBalance(decimalPlaces: 0)
	
	/// Get the underlying number of decimal places that this token represents
	public var decimalPlaces: Int {
		get {
			return balance.decimalPlaces
		}
	}
	
	
	
	// MARK: - Init
	
	/**
	Init a `Token` object that will hold all the necessary data to interact with the Tezos network, and the Dexter exchange
	- parameter icon: An image used to denote the token.
	- parameter name: The long name of the token. e.g. "Tezos"
	- parameter symbol: The short name of the token, or the symbol. e.g. "XTZ"
	- parameter tokenType: The type of the token. e.g. xtz, fa1.2, fa2 etc.
	- parameter decimalPlaces: The number of decimal places this token contains.
	- parameter tokenContractAddress: The KT1 address of the contract (nil if xtz).
	- parameter dexterExchangeAddress: The KT1 address of the contract used to perform exchanges on Dexter (nil if xtz).
	*/
	public init(icon: UIImage?, name: String, symbol: String, tokenType: TokenType, balance: TokenAmount, tokenContractAddress: String?, dexterExchangeAddress: String?) {
		self.icon = icon
		self.name = name
		self.symbol = symbol
		self.tokenType = tokenType
		self.balance = balance
		self.tokenContractAddress = tokenContractAddress
		self.dexterExchangeAddress = dexterExchangeAddress
	}
	
	/**
	Create a `Token` object with all the settings needed for XTZ
	- returns: `Token`
	*/
	public static func xtz() -> Token {
		return Token(icon: nil, name: "Tezos", symbol: "XTZ", tokenType: .xtz, balance: TokenAmount.zeroBalance(decimalPlaces: 6), tokenContractAddress: nil, dexterExchangeAddress: nil)
	}
	
	/**
	Create a `Token` object with all the settings needed for XTZ, with an initial amount. Useful for setting fees.
	- parameter withAmount: The Amount of XTZ to create the `Token` with.
	- returns: `Token`.
	*/
	public static func xtz(withAmount amount: TokenAmount) -> Token {
		return Token(icon: nil, name: "Tezos", symbol: "XTZ", tokenType: .xtz, balance: amount, tokenContractAddress: nil, dexterExchangeAddress: nil)
	}
}
