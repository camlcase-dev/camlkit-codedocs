//
//  InDEXTerService.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 26/11/2020.
//  Copyright © 2020 camlCase Inc. All rights reserved.
//

import Foundation
import os.log


/// InDEXTer is an open source server side aplication developed by camlCase to make fetching data about FA1.2 contracts easier for web and mobile applications.
public class InDEXTerService {
	
	
	/// Types of errors that can be returned
	public enum InDEXTerServiceError: Error {
		case invalidURL
		case invalidToken
		case parsingError
		case serverError(String)
	}
	
	private let networkService: NetworkService
	private let config: TezosNodeClientConfig
	
	
	
	/// Init with a `URLSession` instance, making it possible to mock the responses, and the `baseURL` of the InDEXTer server
	public init(networkService: NetworkService, config: TezosNodeClientConfig) {
		self.networkService = networkService
		self.config = config
	}
	
	/**
	Fetch how much of a given FA1.2 token, that the user owns.
	- parameter forToken: The `Token` object to request the users balance of.
	- parameter andOwner: The tezos address of the owner to query.
	- parameter forceRefresh: Boolean indicating whether to force the cache to refresh the value. Useful when you know the user has performed an action that will change the balance.
	- parameter completion: The completion block that will be called with the resulting `TokenAmount` or `Error`
	*/
	public func fa12Balance(forToken token: Token, andOwner owner: String, forceRefresh: Bool = false, completion: @escaping ((TokenAmount?, ErrorResponse?) -> Void)) {
		guard let tokenAddress = token.tokenContractAddress else {
			completion(nil, ErrorResponse.internalApplicationError(error: InDEXTerServiceError.invalidToken))
			return
		}
		
		var fullURL = config.inDEXTerURL.appendingPathComponent("fa12/\(tokenAddress)/balance/" + owner)
		if forceRefresh {
			fullURL = fullURL.appendingPathComponent("/now")
		}
		fullURL.appendQueryItem(name: "network", value: config.tezosChainName.rawValue)
		
		networkService.request(url: fullURL, isPOST: false, withBody: nil, forReturnType: String.self) { (result) in
			switch result {
				case .success(let stringResponse):
					if let amount = TokenAmount(fromRpcAmount: stringResponse, decimalPlaces: token.decimalPlaces) {
						completion(amount, nil)
						
					} else {
						completion(TokenAmount.zeroBalance(decimalPlaces: token.decimalPlaces), nil)
					}
					
				case .failure(_):
					// Serious network errors will be caught and reported by the NetworkService automatically
					// Its common for inDEXter to return errors, in the cause of an un-revealed wallet address being passed in.
					// For the UI, its better to surpress this and simply return zero, and log any potential issue with the ErrorHandling callback
					completion(TokenAmount.zeroBalance(decimalPlaces: token.decimalPlaces), nil)
			}
		}
	}
	
	/**
	Certain operations on Tezos (such as dexter tokenToXtx exchanges), require the user to give a contract permission to spend their FA1.2 tokens, on their behalf.
	Certain situations when performing exchanges, require knowing how much the current approved allowance is, this allows developers to query that allowance.
	- parameter forToken: The `Token` object to request the users balance of.
	- parameter andOwner: The tezos address of the owner to query.
	- parameter spender: The Tezos address requesting an allowance.
	- parameter completion: The completion block that will be called with the resulting `TokenAmount` or `Error`
	*/
	public func fa12Allowance(forToken token: Token, owner: String, spender: String, completion: @escaping ((TokenAmount?, ErrorResponse?) -> Void)) {
		guard let tokenAddress = token.tokenContractAddress else {
			completion(nil, ErrorResponse.internalApplicationError(error: InDEXTerServiceError.invalidToken))
			return
		}
		
		var fullURL = config.inDEXTerURL.appendingPathComponent("fa12/\(tokenAddress)/allowance/\(owner)/\(spender)")
		fullURL.appendQueryItem(name: "network", value: config.tezosChainName.rawValue)
		
		networkService.request(url: fullURL, isPOST: false, withBody: nil, forReturnType: String.self) { (result) in
			switch result {
				case .success(let stringResponse):
					if let amount = TokenAmount(fromRpcAmount: stringResponse, decimalPlaces: token.decimalPlaces) {
						completion(amount, nil)
						
					} else {
						completion(TokenAmount.zeroBalance(decimalPlaces: 0), nil)
					}
					
				case .failure(_):
					// Serious network errors will be caught and reported by the NetworkService automatically
					// Its common for inDEXter to return errors, in the cause of an un-revealed wallet address being passed in.
					// For the UI, its better to surpress this and simply return zero, and log any potential issue with the ErrorHandling callback
					completion(TokenAmount.zeroBalance(decimalPlaces: token.decimalPlaces), nil)
			}
		}
	}
}
