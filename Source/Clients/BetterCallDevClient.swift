//
//  BetterCallDevClient.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 27/01/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation
import os.log

public class BetterCallDevClient {
	
	public enum BetterCallDevClientError: Error {
		case invalidURL
		case parseError(String)
	}
	
	private let networkService: NetworkService
	private let config: TezosNodeClientConfig
	
	
	public init(networkService: NetworkService, config: TezosNodeClientConfig) {
		self.networkService = networkService
		self.config = config
	}
	
	public func getMoreDetailedError(byHash hash: String, completion: @escaping ((BetterCallDevOperationError?, ErrorResponse?) -> Void)) {
		var url = config.betterCallDevURL
		url.appendPathComponent("v1/opg/" + hash)
		
		networkService.request(url: url, isPOST: false, withBody: nil, forReturnType: [BetterCallDevOperation].self) { (result) in
			switch result {
				case .success(let operations):
					for op in operations {
						if let moreDetailedError = op.moreDetailedError() {
							completion(moreDetailedError, nil)
							return
						}
					}
					
					completion(nil, nil)
					
				case .failure(let error):
					os_log(.error, log: .camlKit, "Parse error: %@", "\(error)")
					completion(nil, ErrorResponse.unknownParseError(error: error))
			}
		}
	}
	
	public func account(forAddress address: String, completion: @escaping ((Result<BetterCallDevAccount, ErrorResponse>) -> Void)) {
		var url = config.betterCallDevURL
		url.appendPathComponent("v1/account/\(config.tezosChainName.rawValue)/\(address)")
		
		networkService.request(url: url, isPOST: false, withBody: nil, forReturnType: BetterCallDevAccount.self, completion: completion)
	}
	
	public func tokenBalances(forAddress address: String, completion: @escaping ((Result<BetterCallDevTokenBalances, ErrorResponse>) -> Void)) {
		var url = config.betterCallDevURL
		url.appendPathComponent("v1/account/\(config.tezosChainName.rawValue)/\(address)/token_balances")
		
		networkService.request(url: url, isPOST: false, withBody: nil, forReturnType: BetterCallDevTokenBalances.self, completion: completion)
	}
	
	public func accountAndTokenBalances(forAddress address: String, completion: @escaping ((Result<(account: BetterCallDevAccount, tokens: BetterCallDevTokenBalances), ErrorResponse>) -> Void)) {
		account(forAddress: address) { [weak self] (result) in
			switch result {
				case .success(let account):
					
					self?.tokenBalances(forAddress: address) { (innerResult) in
						switch innerResult {
							case .success(let tokens):
								completion(Result.success((account: account, tokens: tokens)))
							
							case .failure(let error):
								completion(Result.failure(error))
						}
					}
					
				case .failure(let error):
					completion(Result.failure(error))
			}
		}
	}
	
	// Temporary wrapper around `accountAndTokenBalances` to get all balances as `Token` from BCD instead of InDEXter, so we can deprecate the service
	public func getAllBalances(forAddress address: String, completion: @escaping ((Result<[Token], ErrorResponse>) -> Void)) {
		
		self.accountAndTokenBalances(forAddress: address) { (result) in
			
			switch result {
				case .success(let response):
					var tokens: [Token] = [Token(icon: nil, name: "Tezos", symbol: "XTZ", tokenType: .xtz, balance: response.account.balance, tokenContractAddress: nil, dexterExchangeAddress: nil)]
					for t in response.tokens.balances {
						tokens.append(Token(icon: nil, name: t.name ?? "", symbol: t.symbol ?? "?", tokenType: .fa1_2, balance: t.balance, tokenContractAddress: t.contract, dexterExchangeAddress: nil))
					}
					completion(Result.success(tokens))
				
				case .failure(let errorResponse):
					completion(Result.failure(errorResponse))
			}
		}
	}
}
