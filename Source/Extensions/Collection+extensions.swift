//
//  Collection+extensions.swift
//  camlKit
//
//  Created by Simon Mcloughlin on 26/01/2021.
//  Copyright © 2021 camlCase Inc. All rights reserved.
//

import Foundation

extension Collection {
	
	/// Returns the element at the specified index if it is within bounds, otherwise nil.
	subscript (safe index: Index) -> Element? {
		return indices.contains(index) ? self[index] : nil
	}
}
