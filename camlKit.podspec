Pod::Spec.new do |s|
	s.name             = 'camlKit'
	s.version          = '0.3.17'
	s.summary          = 'camlKit is a native Swift library for interacting with the Tezos Blockchain and other camlCase applications, such as Dexter.'
	s.description      = <<-DESC
	camlKit is a native Swift library, built by camlCase, to make interacting with the Tezos Blockchain easier. camlKit also includes APIs to help developers integrate other camlCase applications, such as Dexter.
						 DESC
	s.homepage         = 'https://gitlab.com/camlcase-dev/camlkit'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'Simon McLoughlin' => 'simon.d.mcl@gmail.com' }
	s.source           = { :git => 'https://gitlab.com/camlcase-dev/camlkit', :tag => s.version.to_s }
	s.ios.deployment_target = '12.0'
	s.swift_version = ['5.0', '5.1', '5.2']
	s.source_files = 'Source/**/*.{swift}'
	s.resources = 'Source/**/*.{js}'

	s.dependency  'BigInt', '5.2.0'
	s.dependency  'Sodium', '0.9.1'
	s.dependency  'secp256k1.swift', '0.1.4'
	s.dependency  'TrustWalletCore', '2.5.6'

	# TrustWalletCore doesn't support MacOS or arm64 yet, and causes cocoapods validation to fail
	s.xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
	s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  end